import numpy as np
import matplotlib.pyplot as plt

# Datos:
# Error del sensor (constante)
sigma_pos = 10
varianza_pos = np.square(sigma_pos)
# Posición real
pos_xyz = np.loadtxt('posicion.dat')
# Velocidad real
vel_xyz = np.loadtxt('velocidad.dat')
# Aceleración real
acc_xyz = np.loadtxt('aceleracion.dat')

# De la matriz de posiciones obtenemos la posición en los distintos ejes
pos_x = pos_xyz[:, 1]
pos_y = pos_xyz[:, 2]
pos_z = pos_xyz[:, 3]
# De la matriz de velociades obtenemos la posición en los distintos ejes
vel_x = vel_xyz[:, 1]
vel_y = vel_xyz[:, 2]
vel_z = vel_xyz[:, 3]
vel = np.vstack((vel_x, vel_y, vel_z))

# De la matriz de aceleraciones obtenemos la posición en los distintos ejes
acc_x = acc_xyz[:, 1]
acc_y = acc_xyz[:, 2]
acc_z = acc_xyz[:, 3]
acc = np.vstack((acc_x, acc_y, acc_z))

# Creamos el vector de medidas, agregando ruido a la posición real
measures_x = pos_x + np.random.normal(0, varianza_pos, len(pos_x))
measures_y = pos_y + np.random.normal(0, varianza_pos, len(pos_y))
measures_z = pos_z + np.random.normal(0, varianza_pos, len(pos_z))

measures_pos = np.vstack((measures_x, measures_y, measures_z))
dt = 1  # Periodo de análisis del sistema, en nuestro caso, 1 segundo
# Matriz A, correspondiente a la dinámina del sistema
# Fila, que a su vez es una matriz, para la dinámica de la posición
A_pos = np.hstack((np.identity(3), np.identity(3) * dt,
                  np.identity(3) * (np.square(dt) / 2)))
# Fila, que a su vez es una matriz, para la dinámica de la velocidad
A_vel = np.hstack((np.zeros((3, 3)), np.identity(3), dt * np.identity(3)))
# Fila, que a su vez es una matriz, para la dinámica de la aceleración
A_acc = np.hstack((np.zeros((3, 3)), np.zeros((3, 3)), np.identity(3)))

A = np.vstack((A_pos, A_vel, A_acc))

# Nos dan como dato la matriz Q (matriz de covarianza)
Q = 0.3 * np.identity(9)

# Para el caso en el que sólo calculamos la pocición
C = np.hstack((np.identity(3), np.zeros((3, 3)), np.zeros((3, 3))))

# Para el caso de ruido gaussiano, la matriz de covarianza del ruido es:
R = np.identity(3) * varianza_pos

# Crear lista para resultado
estimations = []

# Covarianza del error de medición en momento en el que inició el recorrido
P_n0_n0 = np.diag([100, 100, 100, 1, 1, 1, 0.01, 0.01, 0.01])
# Punto en el que inició el recorrido (aproximado)
x_n0_n0 = np.transpose(
    [10.7533, 36.6777, -45.1769, 1.1009, -17.0, 35.7418, -5.7247, 3.4268, 5.2774])

for n in range(len(pos_x)):
    # Calculo el estado actual del sistema utilizando la matriz A (dinámica del sistema) y el estado anterior
    # Debido a que el sistema es analizado de forma periódica, A es constante en el tiempo
    # Esto es la estimación a priori
    x_n1_n0 = np.dot(A, x_n0_n0)
    # Calculo el error  utilizando la estimación de error anterior
    P_n1_n0 = A.dot(P_n0_n0).dot(A.T) + Q
    # Calculo la ganancia de Kalman en función de los datos anteriores
    K_n1 = P_n1_n0.dot(C.T).dot(np.linalg.inv(R + C.dot(P_n1_n0).dot(C.T)))
    # Calculo la nueva x, ahora utilizando la medición
    # Solamente estamos midiendo velocidad, entonces
    y_n = C.dot(np.hstack((measures_pos.T[n], vel.T[n], acc.T[n])))
    x_n1_n1 = x_n1_n0 + K_n1.dot(y_n - C.dot(x_n1_n0))
    # Finalmente calculo la nueva covarianza del error
    P_n1_n1 = (np.identity(9) - K_n1.dot(C)).dot(P_n1_n0)
    # Guardo el valor filtrado para graficarlo después
    estimations.append(x_n1_n1)
    # Seteo los valores para la próxima iteración
    x_n0_n0 = x_n1_n1
    P_n0_n0 = P_n1_n1

estimation_pos_x = np.array(estimations)[:, 0]
estimation_pos_y = np.array(estimations)[:, 1]
estimation_pos_z = np.array(estimations)[:, 2]
estimation_vel_x = np.array(estimations)[:, 3]
estimation_vel_y = np.array(estimations)[:, 4]
estimation_vel_z = np.array(estimations)[:, 5]
estimation_acc_x = np.array(estimations)[:, 6]
estimation_acc_y = np.array(estimations)[:, 7]
estimation_acc_z = np.array(estimations)[:, 8]

f, ax = plt.subplots(3, 3, figsize=(10, 8))
ax[0][0].plot(estimation_pos_x, color='orange', label='Estimaciones', zorder=2)
ax[0][0].plot(pos_x, color='blue', label='Real', zorder=3)
ax[0][0].plot(measures_x, color='grey', zorder=1, label='Medición')
ax[0][0].legend()
ax[0][0].set_title('Posición X')
ax[0][1].plot(estimation_vel_x, color='orange', label='Estimaciones', zorder=2)
ax[0][1].plot(vel_x, color='blue', label='Real', zorder=3)
ax[0][1].legend()
ax[0][1].set_title('Velocidad X')
ax[0][2].plot(acc_x, color='blue', label='Real', zorder=3)
ax[0][2].legend()
ax[0][2].set_title('Aceleración X')

ax[1][0].plot(estimation_pos_y, color='orange', label='Estimaciones', zorder=2)
ax[1][0].plot(pos_y, color='blue', label='Real', zorder=3)
ax[1][0].plot(measures_y, color='grey', zorder=1, label='Medición')
ax[1][0].legend()
ax[1][0].set_title('Posición Y')
ax[1][1].plot(estimation_vel_y, color='orange', label='Estimaciones', zorder=2)
ax[1][1].plot(vel_y, color='blue', label='Real', zorder=3)
ax[1][1].legend()
ax[1][1].set_title('Velocidad Y')
ax[1][2].plot(acc_y, color='blue', label='Real', zorder=3)
ax[1][2].legend()
ax[1][2].set_title('Aceleración Y')

ax[2][0].plot(estimation_pos_z, color='orange', label='Estimaciones', zorder=2)
ax[2][0].plot(pos_z, color='blue', label='Real', zorder=3)
ax[2][0].plot(measures_z, color='grey', zorder=1, label='Medición')
ax[2][0].legend()
ax[2][0].set_title('Posición Z')
ax[2][1].plot(estimation_vel_z, color='orange', label='Estimaciones', zorder=2)
ax[2][1].plot(vel_z, color='blue', label='Real', zorder=3)
ax[2][1].legend()
ax[2][1].set_title('Velocidad Z')
ax[2][2].plot(vel_z, color='blue', label='Real', zorder=3)
ax[2][2].legend()
ax[2][2].set_title('Aceleración Z')

plt.show()

error = np.sqrt(np.square((pos_x - estimation_pos_x) / pos_x) +
                np.square((pos_y - estimation_pos_y) / pos_y) + 
                np.square((pos_z - estimation_pos_z) / pos_z) )

f, ax = plt.subplots()
ax.plot(error)
ax.set_title('Distancia relativa de la predicción al valor real')
plt.show()
